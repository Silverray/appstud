import Contacts from '../components/album'
import React, { Component } from 'react';
import { ScrollView } from 'react-native-gesture-handler';

//Call for fetch data from api
    class App extends Component {
        state = {
            contacts: []
          }

          componentDidMount() {
            fetch('https://afternoon-waters-49321.herokuapp.com/v1/browse/featured-playlists')
            .then(res => res.json())
            .then((data) => {
              this.setState({ contacts: data.playlists.items })
            })
            .catch(console.log)
          }

        render() {
            return (
              <ScrollView style={{backgroundColor: 'black', flex: 0.3}}>
                
                <Contacts contacts={this.state.contacts} />
                
              </ScrollView>
              
            )
            if (this.state.hasError) {
              return <h1>Something went wrong.</h1>;
            }
          }
    
    }

    export default App;


   